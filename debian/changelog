pyml (20231101-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch
  * Bump Standards-Version to 4.6.2

 -- Stéphane Glondu <glondu@debian.org>  Thu, 15 Feb 2024 03:43:49 +0100

pyml (20220905-3) unstable; urgency=medium

  * Include ocamlvars.mk in debian/rules

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2023 07:02:14 +0200

pyml (20220905-2) unstable; urgency=medium

  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Sat, 15 Jul 2023 12:01:46 +0200

pyml (20220905-1) unstable; urgency=medium

  * New upstream release
  * Drop 0001-Use-dune-2.7.patch
  * Disable two failing tests (Closes: #1028869)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 23 Jan 2023 12:07:22 +0100

pyml (20220325-1) unstable; urgency=medium

  * Team upload
  * New upstream release. This release fixes a bug with character conversion
    (closes: #1009430).
  * Remove patch 0002-Fix-79-segfault-when-debug_build-true-is-passed-to-i
    which has been applied upstream.
  * Standards-Version 4.6.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 01 Jun 2022 21:54:04 +0200

pyml (20211015-2) unstable; urgency=medium

  * Fix segfault when ~debug_build:true is passed to initialize

 -- Stéphane Glondu <glondu@debian.org>  Mon, 21 Mar 2022 17:20:22 +0100

pyml (20211015-1) unstable; urgency=low

  [ Stéphane Glondu ]
  * New upstream release
  * Bump Standards-Version to 4.6.0

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Stéphane Glondu <glondu@debian.org>  Mon, 29 Nov 2021 14:31:51 +0100

pyml (20200518-2) unstable; urgency=medium

  * Run tests only on native architectures

 -- Stéphane Glondu <glondu@debian.org>  Sat, 08 Aug 2020 00:03:36 +0200

pyml (20200518-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0

 -- Stéphane Glondu <glondu@debian.org>  Fri, 07 Aug 2020 16:10:29 +0200

pyml (20190626-2) unstable; urgency=medium

  * Fix FTBFS on bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Mon, 06 Jan 2020 16:30:13 +0100

pyml (20190626-1) unstable; urgency=medium

  * Initial release (Closes: #886713)

 -- Stéphane Glondu <glondu@debian.org>  Sun, 29 Dec 2019 08:34:19 +0100
